package com.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EmployeurTest {
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    private Employeur employeurAdd1;
    private Employeur employeurAdd2;
    private Employeur employeurSalaire;
    private Employeur employeurDelete;

    @Before
    public void setUp() {
        entityManagerFactory = Persistence.createEntityManagerFactory("Hibernate");
        entityManager = entityManagerFactory.createEntityManager();

        employeurAdd1 = new Employeur();
        employeurAdd1.setMail("test@test");
        employeurAdd1.setNom_employeur("Michel");
        employeurAdd1.setPrenom_employeur("Rue");
        employeurAdd1.setDate_naissance("2018-11-25");
        employeurAdd1.setSalaire(5000);

        employeurAdd2 = new Employeur();
        employeurAdd2.setMail("test1@test");
        employeurAdd2.setNom_employeur("Jean");
        employeurAdd2.setPrenom_employeur("Michel");
        employeurAdd2.setDate_naissance("2018-11-25");
        employeurAdd2.setSalaire(5000);

        employeurSalaire = new Employeur();
        employeurSalaire.setMail("test2@test");
        employeurSalaire.setNom_employeur("Autre");
        employeurSalaire.setPrenom_employeur("Modifier");
        employeurSalaire.setDate_naissance("2018-11-25");
        employeurSalaire.setSalaire(5000);

        employeurDelete = new Employeur();
        employeurDelete.setMail("test3@test");
        employeurDelete.setNom_employeur("Test");
        employeurDelete.setPrenom_employeur("Delete");
        employeurDelete.setDate_naissance("2018-11-25");
        employeurDelete.setSalaire(5000);

        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("delete from Employeur").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.clear();

        entityManager.getTransaction().begin();
        entityManager.persist(employeurSalaire);
        entityManager.getTransaction().commit();
        entityManager.clear();

        entityManager.getTransaction().begin();
        entityManager.persist(employeurDelete);
        entityManager.getTransaction().commit();
        entityManager.clear();
    }

    @Test
    public void testAddEmployee() {
        entityManager.getTransaction().begin();
        entityManager.persist(employeurAdd1);
        entityManager.getTransaction().commit();
        entityManager.clear();

        entityManager.getTransaction().begin();
        entityManager.persist(employeurAdd2);
        entityManager.getTransaction().commit();
        entityManager.clear();

        Employeur employeur3 = entityManager.find(Employeur.class, employeurAdd1.getMail());
        Employeur employeur4 = entityManager.find(Employeur.class, employeurAdd2.getMail());

        assertTrue(employeurAdd1.equals(employeur3));
        assertTrue(employeurAdd2.equals(employeur4));
    }

    @Test
    public void testModifierSalaire() {
        employeurSalaire = entityManager.find(Employeur.class, employeurSalaire.getMail());
        employeurSalaire.setSalaire(9000);

        entityManager.getTransaction().begin();
        entityManager.merge(employeurSalaire);
        entityManager.getTransaction().commit();
        entityManager.clear();

        Employeur employeur3 = entityManager.find(Employeur.class, employeurSalaire.getMail());
        assertTrue(employeurSalaire.equals(employeur3));
    }

    @Test
    public void testSupprimerEmployeur() {
        entityManager.getTransaction().begin();
        employeurDelete = entityManager.find(Employeur.class, employeurDelete.getMail());
        entityManager.remove(employeurDelete);
        entityManager.getTransaction().commit();
        entityManager.clear();

        Employeur employeur3 = entityManager.find(Employeur.class, employeurDelete.getMail());
        assertFalse(employeurDelete.equals(employeur3));
    }

    @After
    public void tearDown() {
        entityManager.close();
        entityManagerFactory.close();
    }
}