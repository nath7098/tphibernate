package com.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Adresse implements Serializable {

  private String codePostal;
  private String nomVille;
  private long numRue;
  private String nomRue;
  private Enseignant enseignant;

  @Id
  @Column(name = "codePostal", nullable = false)
  public String getCodePostal() {
    return codePostal;
  }

  public void setCodePostal(String codePostal) {
    this.codePostal = codePostal;
  }

  @Id
  @Column(name = "nomVille", nullable = false)
  public String getNomVille() {
    return nomVille;
  }

  public void setNomVille(String nomVille) {
    this.nomVille = nomVille;
  }

  @Id
  @Column(name = "numRue", nullable = false)
  public long getNumRue() {
    return numRue;
  }

  public void setNumRue(long numRue) {
    this.numRue = numRue;
  }

  @Id
  @Column(name = "nomRue", nullable = false)
  public String getNomRue() {
    return nomRue;
  }

  public void setNomRue(String nomRue) {
    this.nomRue = nomRue;
  }

  @OneToOne(mappedBy = "domicile")
  public Enseignant getEnseignant() {return enseignant;}

  public void setEnseignant(Enseignant enseignant) {this.enseignant = enseignant;}


}
