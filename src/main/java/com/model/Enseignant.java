package com.model;

import javax.persistence.*;

@Entity
public class Enseignant {
    private String NSS;
    private String nom;
    private String prenom;
    private Ecole ecole;
    private Adresse domicile;

    public Enseignant(String NSS, String nom, String prenom, Ecole ecole, Adresse domicile) {
        this.NSS = NSS;
        this.nom = nom;
        this.prenom = prenom;
        this.ecole = ecole;
        this.domicile = domicile;
    }

    public Enseignant() {
    }

    public Enseignant(String NSS, String nom, String prenom) {
        this.NSS = NSS;
        this.nom = nom;
        this.prenom = prenom;
    }

    @Id
    @Column(name = "NSS", nullable = false)
    public String getNSS() { return NSS;}

    public void setNSS(String NSS) {this.NSS = NSS;}

    @Basic
    @Column(name = "nom", nullable = false)
    public String getNom() {return nom;}

    public void setNom(String nom) {this.nom = nom;}

    @Basic
    @Column(name = "prenom", nullable = false)
    public String getPrenom() {return prenom;}

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @ManyToOne
    public Ecole getEcole() {return ecole;}

    public void setEcole(Ecole ecole) {this.ecole = ecole;}

    @OneToOne
    public Adresse getDomicile() {return domicile;}

    public void setDomicile(Adresse domicile) {
        this.domicile = domicile;
    }
}
