package com.model;

import javax.persistence.*;

@Entity
@Table(name = "employeur")
public class Employeur {
    private String mail;
    private String nom_employeur;
    private String prenom_employeur;
    private String date_naissance;
    private int salaire;

    public Employeur() {

    }

    @Id
    @Column(name = "mail", nullable = false)
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Basic
    @Column(name = "nom_employeur", nullable = false, length = 255)
    public String getNom_employeur() {
        return nom_employeur;
    }

    public void setNom_employeur(String nom) {
        this.nom_employeur = nom;
    }

    @Basic
    @Column(name = "prenom_employeur", nullable = false, length = 255)
    public String getPrenom_employeur() {
        return prenom_employeur;
    }

    public void setPrenom_employeur(String prenom) {
        this.prenom_employeur = prenom;
    }

    @Basic
    @Column(name = "date_naissance", nullable = false, length = 255)
    public String getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(String dateNaissance) {
        this.date_naissance = dateNaissance;
    }

    @Basic
    @Column(name = "salaire", nullable = false)
    public int getSalaire() {
        return salaire;
    }

    public void setSalaire(int salaire) {
        this.salaire = salaire;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employeur employeur = (Employeur) o;

        if (!mail.equals(employeur.mail)) return false;
        if (salaire != employeur.salaire) return false;
        if (nom_employeur != null ? !nom_employeur.equals(employeur.nom_employeur) : employeur.nom_employeur != null) return false;
        if (prenom_employeur != null ? !prenom_employeur.equals(employeur.prenom_employeur) : employeur.prenom_employeur != null) return false;
        return date_naissance != null ? date_naissance.equals(employeur.date_naissance) : employeur.date_naissance == null;
    }

    @Override
    public int hashCode() {
        int result = mail.hashCode();
        result = 31 * result + (nom_employeur != null ? nom_employeur.hashCode() : 0);
        result = 31 * result + (prenom_employeur != null ? prenom_employeur.hashCode() : 0);
        result = 31 * result + (date_naissance != null ? date_naissance.hashCode() : 0);
        result = 31 * result + salaire;
        return result;
    }
}
