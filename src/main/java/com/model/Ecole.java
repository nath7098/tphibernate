package com.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Ecole {

  private String nom;
  private int nbEleves;
  private List<Enseignant> enseignants;

  public Ecole(String nom, int nbEleves, List<Enseignant> enseignants) {
    this.nom = nom;
    this.nbEleves = nbEleves;
    this.enseignants = enseignants;
  }

  public Ecole() {
  }

  public Ecole(String nom, int nbEleves) {
    this.nom = nom;
    this.nbEleves = nbEleves;
  }

  @Id
  @Column(name = "nom", nullable = false)
  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  @Basic
  @Column(name = "nbEleves")
  public int getNbEleves() {
    return nbEleves;
  }

  public void setNbEleves(int nbEleves) {
    this.nbEleves = nbEleves;
  }

  @OneToMany(mappedBy = "ecole")
  public List<Enseignant> getEnseignants() {return enseignants;}

  public void setEnseignants(List<Enseignant> enseignants) {
    this.enseignants = enseignants;
  }

  public void addEnseignant(Enseignant enseignant) {
    if(!enseignants.contains(enseignant))
      enseignants.add(enseignant);
  }

  public void removeEnseignant(Enseignant enseignant) {
    if(enseignants.contains(enseignant))
      enseignants.remove(enseignant);
  }

}
